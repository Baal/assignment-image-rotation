#ifndef BMP_READING_WRITING
#define BMP_READING_WRITING

#include "image.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

struct bmp_header;

struct bmp_header* bmp_header_construct(struct image ** image);
struct bmp_header* bmp_header_read_from_bmp(FILE* in);
bool bmp_header_write_in_bmp(const FILE* in, const struct bmp_header* header);
uint32_t bmp_header_get_bi_height(const struct bmp_header* header);
uint32_t bmp_header_get_bi_width(const struct bmp_header* header);
size_t bmp_header_get_bOffBits(const struct bmp_header* header);

size_t bmp_header_get_size_bytes();

void bmp_header_destroy(struct bmp_header* header);

#endif
