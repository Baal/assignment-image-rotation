#ifndef FILE_READING_WRITING
#define FILE_READING_WRITING

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_PIXEL_OUT_OF_RANGE
  /* коды других ошибок  */
  };
enum  write_status  {
  WRITE_OK = 0,
  WRITE_OUT_OF_BONDS,
  WRITE_ERROR,
  WRITE_NOTHING
  /* коды других ошибок  */
};

struct image* read_from_file( const FILE* in, const char* file_format);
void write_to_file( const FILE* out, struct image* image, const char* file_format);

#endif
