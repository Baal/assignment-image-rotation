#ifndef IMAGE
#define IMAGE

#include <stdbool.h>
#include <stdint.h>

struct pixel;
struct maybe_pixel;
struct image;

struct maybe_pixel* maybe_pixel_get_from_image(const struct image* image, uint64_t width, uint64_t height);
bool image_set_pixel(struct image* image, uint32_t width, uint32_t height, const struct pixel* p);
struct image* image_construct(uint64_t width, uint64_t height);
void image_free(struct image* image);
uint64_t image_get_width(const struct image* image);
uint64_t image_get_height(const struct image* image);
bool maybe_pixel_get_valid(const struct maybe_pixel* pixel);
const struct pixel * maybe_pixel_get_value(const struct maybe_pixel * pixel);
uint32_t pixel_get_size();

struct pixel* image_get_row(struct image* image, uint32_t rows);
void pixel_destroy_padding_row(struct pixel* p);

void pixel_print(const struct pixel* p);

void destroy_image(struct image* img);
void pixel_destroy(struct pixel* p);
void maybe_pixel_destroy(struct maybe_pixel* p);

#endif
