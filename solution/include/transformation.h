#ifndef ROTATION
#define ROTATION

#include "image.h"

struct image* image_transform(const struct image* image, struct image* new_image, char* transformation_mod);

#endif
