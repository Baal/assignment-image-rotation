#include "bmp_header.h"
#include "image.h"
#include <errno.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct bmp_header
{
        struct bfTypeStruct {uint8_t first; uint8_t second;} bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
};
#pragma pack(pop)

struct bmp_header* bmp_header_construct(struct image ** image){
    struct bmp_header* header = (struct bmp_header*)malloc(bmp_header_get_size_bytes());

    const struct image* c_image = *image;
    uint32_t width  = image_get_width(c_image);
    uint32_t height = image_get_height(c_image);

    uint32_t width_bytes = width * pixel_get_size();
    uint32_t padding = width_bytes % 4;

    uint32_t image_size_bytes = height * (width_bytes + padding);
    uint32_t file_size_bytes = image_size_bytes + bmp_header_get_size_bytes();

    header -> bfType = (struct bfTypeStruct){'B', 'M'};
    header -> bfileSize = file_size_bytes;
    header -> bfReserved = 0;
    header -> bOffBits = bmp_header_get_size_bytes();
    header -> biSize = 40;
    header -> biWidth = width;
    header -> biHeight = height;
    header -> biPlanes = 1;
    header -> biBitCount = 24;
    header -> biCompression = 0;
    header -> biSizeImage = image_size_bytes;
    header -> biXPelsPerMeter = 2834;
    header -> biYPelsPerMeter = 2834;
    header -> biClrUsed = 0;
    header -> biClrImportant = 0;

    return header;
}

struct bmp_header* bmp_header_read_from_bmp(FILE* in){
    struct bmp_header* header = (struct bmp_header*)malloc(bmp_header_get_size_bytes());
    
    fread( header, sizeof( struct bmp_header ), 1, in );

    switch(errno){
        case EACCES: {fprintf(stderr, "EACCES \n"); return NULL;}
        case EFBIG: {fprintf(stderr, "EFBIG \n"); return NULL;}
        case EISDIR: {fprintf(stderr, "EISDIR \n"); return NULL;}
        case ENAMETOOLONG: {fprintf(stderr, "ENAMETOOLONG \n"); return NULL;}
        case ENFILE: {fprintf(stderr, "ENFILE \n"); return NULL;}
        case ENOENT: {fprintf(stderr, "ENOENT \n"); return NULL;}
    }
    
    fseek(in, header -> bOffBits, SEEK_SET);
    return header;
}

bool bmp_header_write_in_bmp(const FILE* out, const struct bmp_header* header){
    if (out == NULL) fprintf(stderr, "out_doesn't_valid_header");
    if (header == NULL) fprintf(stderr, "header_doesn't_valid_header");
    bool ans = fwrite(header, bmp_header_get_size_bytes(), 1, (FILE*)out) == 1;
    return ans;
}

uint32_t bmp_header_get_bi_height(const struct bmp_header* header){
    return header -> biHeight;
}
uint32_t bmp_header_get_bi_width(const struct bmp_header* header){
    return header -> biWidth;
}

size_t bmp_header_get_size_bytes(){
    return sizeof(struct bmp_header);
}

size_t bmp_header_get_bOffBits(const struct bmp_header* header){
    return header -> bOffBits;
}

void bmp_header_destroy(struct bmp_header* header){
    free(header);
}
