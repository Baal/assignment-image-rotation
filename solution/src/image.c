#include "image.h"
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//описывает пиксель (система RGB)
#pragma pack(push, 1)
struct pixel{ 
  uint8_t b, g, r; 
};
#pragma pack(pop)

//используется для возвората из функций
struct maybe_pixel{
  bool valid;
  struct pixel* value;
};

//хранит само изображение и его размеры
//data - массив вида height раз по width (это должно поддерживаться в file_reading)
struct image {
  uint32_t width, height;
  struct pixel* data;
};

static struct maybe_pixel* some_pixel( struct pixel p ) {
  struct maybe_pixel* ans = (struct maybe_pixel*)malloc(sizeof(struct maybe_pixel));
  ans -> value = &p;
  ans -> valid = true;
  return ans;
}

struct maybe_pixel none_pixel = { 0 };
struct pixel padding_pixel = {0, 0 , 0};

//внутренние функция модуля
static uint32_t get_index_for_pixel(const struct image* image, uint32_t w, uint32_t h){
  return (h * (image -> width) ) + w;
}

static bool is_correct_coord(const struct image* image, uint32_t width, uint32_t height){
  if ((0 > width)  || (width  >= (image -> width)))  {return false;}
  if ((0 > height) || (height >= (image -> height))) {return false;}
  return true;
}

static struct maybe_pixel* get_maybe_pixel(const struct image* image, uint64_t width, uint64_t height){
  if (is_correct_coord(image, width, height) == false) {return &none_pixel;}

  uint32_t index = get_index_for_pixel(image, width, height);
  struct maybe_pixel* ans = some_pixel((image -> data)[index]);
  return ans;
}


static bool set_pixel_in_image(struct image* image, uint32_t width, uint32_t height, struct pixel p){
  if (is_correct_coord(image, width, height) == false) return false;

  uint32_t index = get_index_for_pixel(image, width, height);
  (image -> data)[index] = p;
  return true;
}

struct image* construct_image_from_sizes(uint32_t width, uint32_t height){
  struct pixel* data = (struct pixel*) malloc(sizeof(struct pixel) * width * height);
  struct image* ans  = (struct image*) malloc(sizeof(struct image));
  ans -> data = data;
  ans -> height = height;
  ans -> width = width;
  return ans;
}

//функции для вызова вне модуля
struct maybe_pixel* maybe_pixel_get_from_image(const struct image* image, uint64_t width, uint64_t height){
  return get_maybe_pixel(image, width, height);
}
bool image_set_pixel(struct image* image, uint32_t width, uint32_t height, const struct pixel* p){
  if (p == NULL) {printf("PIXEL_IN_SET_IS_NULL "); return false;}
  return set_pixel_in_image(image, width, height, *p);
}
struct image* image_construct(uint64_t width, uint64_t height){
  return construct_image_from_sizes(width, height);
}
void image_free(struct image* image){
  free(image -> data);
  free(image);
}
uint64_t image_get_width(const struct image* image){
  return (uint64_t)(image -> width);
}
uint64_t image_get_height(const struct image* image){
  return (uint64_t)(image -> height);
}
bool maybe_pixel_get_valid(const struct maybe_pixel* pixel){
  return pixel -> valid;
}
const struct pixel* maybe_pixel_get_value(const struct maybe_pixel* pixel){
  return pixel -> value;
}

struct pixel* image_get_row(struct image* image, uint32_t rows){
  return (image -> data + (image -> width) * rows);
}

void pixel_destroy(struct pixel* p){
  free(p);
}

void destroy_image(struct image* img){
  if (img == NULL) return;
  free(img -> data);
  free(img);
}
void maybe_pixel_destroy(struct maybe_pixel* p){
  free(p);
}
uint32_t pixel_get_size(){
  return sizeof(struct pixel);
}

void pixel_print(const struct pixel* p){
  printf("|| %u %u %u ||", p -> b, p -> g, p -> r);
}

void pixel_destroy_padding_row(struct pixel* p){
  free(p);
}
