#include "bmp_header.h"
#include "image.h"
#include <errno.h>
#include <file_reading_writing.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*  deserializer   */

static size_t get_padding_size(size_t width){
  return width % 4;
}

//in -> image
struct image* from_bmp( const FILE* in){
  struct bmp_header* header = bmp_header_read_from_bmp((FILE*)in);
  if (header == NULL) {
    fprintf(stderr, "/n failed to read a header from bmp file\n"); 
    return NULL;
  }

  uint32_t width  = bmp_header_get_bi_width(header);
  uint32_t height = bmp_header_get_bi_height(header);

  size_t padding = get_padding_size(width);
  
  struct image* image = image_construct(width, height);
  if (image == NULL) fprintf(stderr, "image_construct_dont_work ");

  fseek((FILE*)in, bmp_header_get_bOffBits(header), SEEK_SET);
  for (uint32_t rows = 0; rows < height; ++rows){
    fread(image_get_row(image, rows), pixel_get_size(), width, (FILE*)in);

    fseek((FILE*)in, padding, SEEK_CUR); //чтение мусора (просто сдвигаем указатель)
  }
  bmp_header_destroy(header);

  switch(errno){
        case EACCES: {fprintf(stderr, "\nfailed reading image from bmp: EACCES \n"); destroy_image(image); return NULL;}
        case EFBIG: {fprintf(stderr, "\nfailed reading image from bmp: EFBIG \n"); destroy_image(image); return NULL;}
        case EISDIR: {fprintf(stderr, "\nfailed reading image from bmp: EISDIR \n"); destroy_image(image); return NULL;}
        case ENAMETOOLONG: {fprintf(stderr, "\nfailed reading image from bmp: ENAMETOOLONG \n"); destroy_image(image); return NULL;}
        case ENFILE: {fprintf(stderr, "\nfailed reading image from bmp: ENFILE \n"); destroy_image(image); return NULL;}
        case ENOENT: {fprintf(stderr, "\nfailed reading image from bmp: ENOENT \n"); destroy_image(image); return NULL;}
    }

  return image;
}

/*  serializer   */


//image -> out
enum write_status to_bmp( const FILE* out, struct image** image ){
  struct image* img = *image;

  struct bmp_header* header = bmp_header_construct(image);

  size_t width  = (size_t)image_get_width(img);
  size_t height = (size_t)image_get_height(img);

  size_t padding = get_padding_size((size_t)width);

  bmp_header_write_in_bmp(out, header);

  fseek((FILE*)out, bmp_header_get_bOffBits(header), SEEK_SET);
  for (size_t rows = 0; rows < height; ++rows){
    fwrite(image_get_row(img, rows), pixel_get_size(), width, (FILE*)out);
    
    fwrite(image_get_row(img, rows), 1, padding, (FILE*)out); //запись мусора
  }

  bmp_header_destroy(header);

  switch(errno){
        case EACCES: {fprintf(stderr, "\nfailed writing image to bmp: EACCES \n"); return WRITE_ERROR;}
        case EFBIG: {fprintf(stderr, "\nfailed writing image to bmp: EFBIG \n"); return WRITE_ERROR;}
        case EISDIR: {fprintf(stderr, "\nfailed writing image to bmp: EISDIR \n"); return WRITE_ERROR;}
        case ENAMETOOLONG: {fprintf(stderr, "\nfailed writing image to bmp: ENAMETOOLONG \n"); return WRITE_ERROR;}
        case ENFILE: {fprintf(stderr, "\nfailed writing image to bmp: ENFILE \n"); return WRITE_ERROR;}
        case ENOENT: {fprintf(stderr, "\nfailed writing image to bmp: ENOENT \n"); return WRITE_ERROR;}
    }

  return WRITE_OK;
}

//функции для вызова вне модуля
struct image* read_from_file( const FILE* in, const char* file_format){
  if (strcmp(file_format, "bmp") == 0) return from_bmp(in);

  fprintf(stderr, "NOT_SUPPORTED_FILE_READING_FORMAT \n");
  return NULL;
}
void write_to_file( const FILE* out, struct image* image, const char* file_format){
  if (out == NULL) fprintf(stderr, "cannot_write_to_NULL_file \n");
  enum write_status res = WRITE_NOTHING;
  if (strcmp(file_format, "bmp") == 0) res = to_bmp(out, &image);

  switch(res){
    case(WRITE_OK): {return;}
    case(WRITE_ERROR): {fprintf(stderr, "/n error during writing to file\n"); return;}
    case(WRITE_OUT_OF_BONDS): {fprintf(stderr, "/n writing out of bonds during writing to file\n"); return;}
    case(WRITE_NOTHING): {fprintf(stderr, "NOT_SUPPORTED_FILE_WRITING_FORMAT \n"); return;}
  }
}
