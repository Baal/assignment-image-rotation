#include "bmp_header.h"
#include "file_opening_closing.h"
#include "file_reading_writing.h"
#include "image.h"
#include "transformation.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    //(void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc != 3) {printf("wrong number of arguments "); return 0;}

    const char* name_in  = argv[1];
    const char* name_out = argv[2];

    struct file_or_error* in  = file_or_error_open_from_name(name_in,  "rb");
    if (file_or_error_get_valid(in)  != true) {printf("in_dont_valid");  return -1;}

    struct image* original = read_from_file(file_or_error_get_value(in), "bmp");
    file_or_error_close_this_file(file_or_error_get_value(in));

    struct image* result = NULL;
    result = image_transform(original, result, "rotate_90_anticlockwise");

    struct file_or_error* out = file_or_error_open_from_name(name_out, "wb");
    write_to_file(file_or_error_get_value(out), result, "bmp"); 
    file_or_error_close_this_file(file_or_error_get_value(out));
    
    destroy_image(original);
    destroy_image(result);

    file_or_error_destroy(in);
    file_or_error_destroy(out);

    return 0;
}
