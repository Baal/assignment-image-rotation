#include "file_opening_closing.h"
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct file_or_error{
  FILE* file;
  enum file_status status;
};

const struct file_or_error none_file = { .file = NULL, .status = FILE_ERROR };

static struct file_or_error* some_file( FILE * file ) {
  struct file_or_error* ans = (struct file_or_error*)malloc(sizeof(struct file_or_error));
  ans -> file = file;
  ans -> status = FILE_OK;
  return ans;
}

void file_or_error_destroy(struct file_or_error* file){
  free(file);
}

//внутренние функции модуля
static struct file_or_error* open_from_name(const char* name, const char* mod){
  FILE* file = fopen(name, mod);

  if (file != NULL) return some_file(file);

  struct file_or_error* ans = some_file(file);

  switch(errno){
    case EACCES: {fprintf(stderr, "EACCES \n"); ans -> status = FILE_NO_ACCES; return ans;}
    case EFBIG: {fprintf(stderr, "EFBIG \n");ans -> status = FILE_TOO_BIG; return ans;}
    case EISDIR: {fprintf(stderr, "EISDIR \n");ans -> status = FILE_IS_CATALOG; return ans;}
    case ENAMETOOLONG: {fprintf(stderr, "ENAMETOOLONG \n");ans -> status = FILE_NAME_TOO_LONG; return ans;}
    case ENFILE: {fprintf(stderr, "ENFILE \n");ans -> status = FILE_TOO_MANY_FILES_IN_SYSTEM; return ans;}
    case ENOENT: {fprintf(stderr, "ENOENT \n");ans -> status = FILE_NOT_FOUND; return ans;}
  }

  fprintf(stderr, "UNKNOWN ERROR DURING OPENING FILE \n"); ans -> status = FILE_ERROR;
  return ans;
}

enum file_status close_file(FILE* file){
  if (file == NULL) return FILE_ERROR;
  if (fclose(file) == 0) {return FILE_OK;}

  enum file_status ans = FILE_OK;

  switch(errno){
    case EACCES: {fprintf(stderr, "EACCES \n"); ans = FILE_NO_ACCES; return ans;}
    case EFBIG: {fprintf(stderr, "EFBIG \n"); ans = FILE_TOO_BIG; return ans;}
    case EISDIR: {fprintf(stderr, "EISDIR \n"); ans = FILE_IS_CATALOG; return ans;}
    case ENAMETOOLONG: {fprintf(stderr, "ENAMETOOLONG \n"); ans = FILE_NAME_TOO_LONG; return ans;}
    case ENFILE: {fprintf(stderr, "ENFILE \n"); ans = FILE_TOO_MANY_FILES_IN_SYSTEM; return ans;}
    case ENOENT: {fprintf(stderr, "ENOENT \n"); ans = FILE_NOT_FOUND; return ans;}
  }

  fprintf(stderr, "UNKNOWN ERROR DURING CLOSING FILE \n"); ans = FILE_ERROR;
  
  return ans;
}



//функции для вызова вне модуля
struct file_or_error* file_or_error_open_from_name(const char* name, const char* mod){
    return open_from_name(name, mod);
}
bool file_or_error_close_this_file(FILE* in){
  if (close_file(in) == FILE_OK) return true;
  return false;
}
bool file_or_error_get_valid(struct file_or_error* f){
  return (f -> status == FILE_OK);
}
FILE* file_or_error_get_value(struct file_or_error* f){
  return f -> file;
}
