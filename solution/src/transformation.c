#include "image.h"
#include "transformation.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

static struct image* translate_to_new_image_rotate(const struct image* old_image, struct image* new_image){
    for (uint64_t curr_height = 0; curr_height < image_get_height(old_image); ++curr_height)
        for(uint64_t curr_width = 0; curr_width < image_get_width(old_image); ++curr_width){
            uint64_t new_curr_height = curr_width;
            uint64_t new_curr_width  = image_get_width(new_image) - curr_height - 1;

            struct maybe_pixel* curr_pixel = maybe_pixel_get_from_image(old_image, curr_width, curr_height);
            image_set_pixel(new_image, new_curr_width, new_curr_height, maybe_pixel_get_value(curr_pixel));
            maybe_pixel_destroy(curr_pixel);
        }

    return new_image;
}

static struct image* rotate(const struct image* image, struct image* new_image){
    if (image == NULL) printf("original image is cursed ");

    uint64_t new_height = image_get_width(image);
    uint64_t new_width = image_get_height(image);

    new_image = image_construct(new_width, new_height);

    return translate_to_new_image_rotate(image, new_image);
}

//функция общего вида
struct image* image_transform(const struct image* image, struct image* new_image, char* transformation_mod){
    if (strcmp(transformation_mod, "rotate_90_anticlockwise") == 0) return rotate(image, new_image);

    fprintf(stderr, "UNKNOWN_TRANSFORMATION_MOD \n");
    return NULL;
}
